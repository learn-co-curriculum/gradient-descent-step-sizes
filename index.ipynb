{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Gradient Descent Step Sizes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Learning Objectives"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Understand how the slope of the cost curve at a given point can indicate the size and direction of our ideal next step\n",
    "* Understand how to update our regression line values iteratively using this slope \n",
    "* Understand that the tangent line indicates the slope of our function at a given point"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Our approach so far"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the last section, we saw our process for improviding regression lines.  We started with a simple regression line of the form, $y = mx + b $ to predict an output given an input.  Then, we define our errors to be the differences between the output that our regression line predicts and the actual value of an error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![regression-scatter.png](./regression-scatter.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then quantify how well our regression line maps to our data by squaring all of the errors (to eliminate negative values) and adding these squares together, to get our residual sum of squares (RSS).  Armed with a number that describes \"goodness of fit\", we iteratively try new regression lines by adjusting our y-intercept value, $b$, or slope value, $m$, and assessing goodness of fit.  By finding the values $m$ and $b$ that minimize the RSS, we can find our \"best fit line\".  In our cost function below, you can see the different values $b$ and the RSS that these values produce (given a specific value $m$).  The bottom of the blue curve displays the $b$ value that produces the lowest RSS."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<script>requirejs.config({paths: { 'plotly': ['https://cdn.plot.ly/plotly-latest.min']},});if(!window.Plotly) {{require(['plotly'],function(plotly) {window.Plotly=plotly;});}}</script>"
      ],
      "text/vnd.plotly.v1+html": [
       "<script>requirejs.config({paths: { 'plotly': ['https://cdn.plot.ly/plotly-latest.min']},});if(!window.Plotly) {{require(['plotly'],function(plotly) {window.Plotly=plotly;});}}</script>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.plotly.v1+json": {
       "data": [
        {
         "mode": "line",
         "name": "data",
         "text": [],
         "x": [
          70,
          80,
          90,
          100,
          110,
          120,
          130,
          140
         ],
         "y": [
          10852,
          9690,
          9128,
          9166,
          9804,
          11042,
          12880,
          15318
         ]
        }
       ],
       "layout": {}
      },
      "text/html": [
       "<div id=\"1d895a39-ed8b-4cc1-8d18-68513ba7c281\" style=\"height: 525px; width: 100%;\" class=\"plotly-graph-div\"></div><script type=\"text/javascript\">require([\"plotly\"], function(Plotly) { window.PLOTLYENV=window.PLOTLYENV || {};window.PLOTLYENV.BASE_URL=\"https://plot.ly\";Plotly.newPlot(\"1d895a39-ed8b-4cc1-8d18-68513ba7c281\", [{\"x\": [70, 80, 90, 100, 110, 120, 130, 140], \"y\": [10852, 9690, 9128, 9166, 9804, 11042, 12880, 15318], \"mode\": \"line\", \"name\": \"data\", \"text\": []}], {}, {\"showLink\": true, \"linkText\": \"Export to plot.ly\"})});</script>"
      ],
      "text/vnd.plotly.v1+html": [
       "<div id=\"1d895a39-ed8b-4cc1-8d18-68513ba7c281\" style=\"height: 525px; width: 100%;\" class=\"plotly-graph-div\"></div><script type=\"text/javascript\">require([\"plotly\"], function(Plotly) { window.PLOTLYENV=window.PLOTLYENV || {};window.PLOTLYENV.BASE_URL=\"https://plot.ly\";Plotly.newPlot(\"1d895a39-ed8b-4cc1-8d18-68513ba7c281\", [{\"x\": [70, 80, 90, 100, 110, 120, 130, 140], \"y\": [10852, 9690, 9128, 9166, 9804, 11042, 12880, 15318], \"mode\": \"line\", \"name\": \"data\", \"text\": []}], {}, {\"showLink\": true, \"linkText\": \"Export to plot.ly\"})});</script>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import plotly\n",
    "from plotly.offline import init_notebook_mode, iplot\n",
    "from graph import m_b_trace, trace_values, plot\n",
    "init_notebook_mode(connected=True)\n",
    "b_values = list(range(70, 150, 10))\n",
    "rss = [10852, 9690, 9128, 9166, 9804, 11042, 12880, 15318]\n",
    "cost_curve_trace = trace_values(b_values, rss, mode=\"line\")\n",
    "plot([cost_curve_trace])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we don't want to try **all** of the different values for a regression line, $m$ and $b$.  Doing so takes too long, as there are too many possibilities. Especially as if our regression formulas will only get more complicated.  This technique doesn't work.\n",
    "\n",
    "So in the last lesson, instead of checking all of the values, we checked in increments of 10 to see if our changing $b$ value produced a higher or lower RSS.  \n",
    "\n",
    "| b        | residual sum of squared           | \n",
    "| ------------- |:-------------:| \n",
    "| 140 | 15318 | \n",
    "| 130      |12880| \n",
    "| 120      |11042 | \n",
    "| 110      |9804| \n",
    "|100 | 9166\n",
    "|90 | 9128\n",
    "|80 | 9690\n",
    "|70| 10852"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looking at the chart above, we settled in on a value of $b$ between 90 and 100, as that is where produced the corresponding lowest RSS.\n",
    "\n",
    "> Remember, that ultimately we want to change both of our parameters to find the \"best fit\" line, but for now we just change one to make things easier."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Still, things are not so simple"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above approach of checking our RSS every so often works pretty well.  The only thing we want to differently is make sure we take our steps with more certainty that we will be approaching our minimum RSS.  Adjusting the y-intercept value and then checking the effect is a bit like trying to fly plane by just moving sitting down and pressing buttons.  Is there a way that even with our first change, we can rest assured we're moving in the right direction?  And how do we know how large a change to make to our y-intercept with each step?  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's call each of these changes a step, and the size of the step our step size.  Our task is to have our step sizes get us to our RSS quickly and without overshooting the mark."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](https://bossip.files.wordpress.com/2014/11/aden-and-cree-580x435.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The slope of the cost curve tells us our step size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Believe it or not, we can determine how large our step size should be looking the slope of our cost function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In fact, let's just look at one part of it.  Looking at just one part of our cost function below, do you see a way that even without knowing the minimum value, we can still get a sense of the direction and size of our next step?  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<script>requirejs.config({paths: { 'plotly': ['https://cdn.plot.ly/plotly-latest.min']},});if(!window.Plotly) {{require(['plotly'],function(plotly) {window.Plotly=plotly;});}}</script>"
      ],
      "text/vnd.plotly.v1+html": [
       "<script>requirejs.config({paths: { 'plotly': ['https://cdn.plot.ly/plotly-latest.min']},});if(!window.Plotly) {{require(['plotly'],function(plotly) {window.Plotly=plotly;});}}</script>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.plotly.v1+json": {
       "data": [
        {
         "mode": "line",
         "name": "data",
         "text": [],
         "x": [
          70,
          80,
          90
         ],
         "y": [
          10852,
          9690,
          9128
         ]
        }
       ],
       "layout": {}
      },
      "text/html": [
       "<div id=\"90b1df03-8cab-4010-a92b-b9cb063fcf34\" style=\"height: 525px; width: 100%;\" class=\"plotly-graph-div\"></div><script type=\"text/javascript\">require([\"plotly\"], function(Plotly) { window.PLOTLYENV=window.PLOTLYENV || {};window.PLOTLYENV.BASE_URL=\"https://plot.ly\";Plotly.newPlot(\"90b1df03-8cab-4010-a92b-b9cb063fcf34\", [{\"x\": [70, 80, 90], \"y\": [10852, 9690, 9128], \"mode\": \"line\", \"name\": \"data\", \"text\": []}], {}, {\"showLink\": true, \"linkText\": \"Export to plot.ly\"})});</script>"
      ],
      "text/vnd.plotly.v1+html": [
       "<div id=\"90b1df03-8cab-4010-a92b-b9cb063fcf34\" style=\"height: 525px; width: 100%;\" class=\"plotly-graph-div\"></div><script type=\"text/javascript\">require([\"plotly\"], function(Plotly) { window.PLOTLYENV=window.PLOTLYENV || {};window.PLOTLYENV.BASE_URL=\"https://plot.ly\";Plotly.newPlot(\"90b1df03-8cab-4010-a92b-b9cb063fcf34\", [{\"x\": [70, 80, 90], \"y\": [10852, 9690, 9128], \"mode\": \"line\", \"name\": \"data\", \"text\": []}], {}, {\"showLink\": true, \"linkText\": \"Export to plot.ly\"})});</script>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import plotly\n",
    "from plotly.offline import init_notebook_mode, iplot\n",
    "from graph import m_b_trace, trace_values, plot\n",
    "init_notebook_mode(connected=True)\n",
    "b_values = list(range(70, 150, 10)[:3])\n",
    "rss = [10852, 9690, 9128, 9166, 9804, 11042, 12880, 15318][:3]\n",
    "cost_curve_trace = trace_values(b_values, rss, mode=\"line\")\n",
    "plot([cost_curve_trace])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You likely can if you imagine yourself standing on your cost curve.  Even with your eyes closed, you could tell simply by the way you were tilting whether to walk forwards or backwards to approach the bottom of the cost curve.  If the slope tilts downwards at that point  we walk forward, and if the slope tilts upwards at that point we walk backwards.  Then the steeper the tilt, the larger the step we should take as the further away we are from our cost curve.  \n",
    "\n",
    "So by looking to the tilt of a cost curve at a given point, we can discover the direction of our next step and how large of step to take.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stepping according to the slope"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](./gradient-tangents.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can follow our technique with more precision by adding some numbers to our slope.  The slope of curve at any given point is equal to the slope of the tangent line at that point.  By tangent line, we mean the line that just barely touches the curve at that point.  In the above graph, the orange, green, and red lines are tangent to our cost curve at the points where $b$ equals 70, 84, and 90 respectively.  The slopes of our tangent lines, and therefore the slopes of the cost curves at those points, are labeled above.  \n",
    "\n",
    "The whole point of looking at the slope is because it supposedly tells us the size and direction of our next step, and thus tells us how to change our value of $b$.  Let's see how this works.\n",
    "\n",
    "We can use the following formula for approaching our $b$ finding the ideal $b$: \n",
    "1.  Randomly choose a value of $b$, and then \n",
    "2.  Update $b$ with the formula $ b = (-.1) * slope_{b = i} + b_i$.\n",
    "\n",
    "All that formula says, is choose a value of $b$, and then move it to be a small number, -.1 times the slope.  This way, the larger the slope, the larger the step.  And the negative means we will always move in the opposite direction of the slope (that is, when the tangent line points downwards move forwards). \n",
    "\n",
    "Let's see an example.  We randomly choose our value of $b$ to equal 70.  Then:\n",
    "\n",
    "* $b_{t=0} = 70 $\n",
    "* $b_{t=1} = (-.1) * -146.17  + 70 = 14.61 + 70 = 84.61 $\n",
    "* $b_{t=2} = (-.1) * -58.51 + 85 = 5.851 + 85 = 90.851 $\n",
    "* $b_{t=3} = (-.1) * -21.07 + 90.85 = 90.851 + 2.107 $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So notice that we don't update our values of $b$ by just adding or subtracting the slope at that point.  Doing so would be too drastic.  Instead we multiply by the slope by a fraction -- in this case -- $.1$ which is called a **learning rate**.  This way, the steeper slope of the tangent line, the more we change in $b$, but we still make sure we are not changing our regression lines too drastically.  \n",
    "> We should point out that, in general, learning rates are much smaller than what we saw here (.001 is more typical), but we used this learning rate just for demonstration purposes.   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also notice that this technique is pretty magical.  By looking at the tangent line at each point, we no longer are  changing our $b$ value and just hoping that it has the correct impact on our RSS.  This is because, for one, the slope of the tangent line points us in the right direction.  And as you can see above, our technique properly adjusts the amount to change the $b$ value by without even knowing where ideal $b$ value is.  When our $b$ was far away from the ideal $b$ value our formula had us increase our $b$ by 14, and in just three steps we were only updating our $b$ value by 2, as we approached the $b$ that minimizes our RSS.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Summary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "We started this section with saying that we wanted a technique to find a $b$ value that would minimize our RSS, given a value of $m$.  We did not want to simply try all of the values of $b$ as doing so would be inefficient.  Instead, we went with the approach of gradient descent, where we try variations of regression lines by iteratively changing our $b$ variable and assessing our RSS to see if we are making progress.\n",
    "\n",
    "In this lesson, we focused in on how to know which direction to alter a given variable, $m$ or $b$, as well as a technique for determining the size of the change to one of our variables.  We used the line tangent to our cost curve at a given point to indicate the direction and size of the update to $b$.  The further away, the steeper the curve and thus the larger the step we would want to take.  Appropriately, our tangent line slope would have us take a larger step.  And the closer we are to the ideal $b$ value, the flatter the tangent line to the curve, and the smaller a step we would take. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
